#!/usr/bin/env bash

    if [ "$1" == "up" ]; then
    	cp .env.example .env

       	docker-compose build

        docker-compose up -d

        docker-compose exec app composer install

        docker-compose exec app php artisan route:cache

        docker-compose exec app php artisan config:cache

		##docker-compose run --rm -w /var/www/html -v $PWD:/var/www/html node npm install


    elif [ "$1" == "down" ]; then
    	docker-compose down
    fi