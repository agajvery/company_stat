<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 9:06 AM
 */

namespace App\Domain\Adapter;

use Illuminate\Support\Facades\Cache;

class LaravelCache implements CacheInterface
{

    public function get($key)
    {
        return Cache::get($key);
    }

    public function set(string $key, $value, int $ttl)
    {
        return Cache::add($key, $value, $ttl);
    }
}