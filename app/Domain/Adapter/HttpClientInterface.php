<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 12:46 PM
 */

namespace App\Domain\Adapter;


interface HttpClientInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    public function request(string $method, string $url, array $params = []);
}