<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:50 AM
 */

namespace App\Domain\Adapter;


interface CacheInterface
{
    public function get($key);

    public function set(string $key, $value, int $ttl);
}