<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 1:05 PM
 */

namespace App\Domain\Adapter;


use GuzzleHttp\Client;

class GuzzleHttpClient implements HttpClientInterface
{

    public function request(string $method, string $url, array $params = [])
    {
        $client = new Client();
        $response = $client->request($method, $url, $params);

        return [
            'code' => $response->getStatusCode(),
            'body' => $response->getBody(),
            'headers' => $response->getHeaders()
        ];
    }
}