<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 1:55 PM
 */

namespace App\Domain\Service;

use App\Domain\Repository\CompanyStatisticInterface as RepositoryCompanyStatisticInterface;

class CompanyStatistic implements CompanyStatisticInterface
{
    private $repository;

    public function __construct(RepositoryCompanyStatisticInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getStatistic(string $companyCode, string $dateFrom, string $dateTo)
    {

        $statistic = [];
        $dataSet = $this->repository->getStatistic($companyCode, $dateFrom, $dateTo);

        if ($dataSet) {
            $displayKeys = $dataSet['column_names'];

            foreach ($dataSet['data'] as $row) {
                $statistic[] = array_combine($displayKeys, array_intersect_key($row, $displayKeys)) ;
            }
        }

        return $statistic;
    }
}