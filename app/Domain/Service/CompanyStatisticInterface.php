<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 1:41 PM
 */

namespace App\Domain\Service;


interface CompanyStatisticInterface
{
    public function getStatistic(string $companyCode, string $dateFrom, string $dateTo);
}