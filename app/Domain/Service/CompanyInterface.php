<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:45 AM
 */

namespace App\Domain\Service;

use App\Domain\Entity\Company as CompanyEntity;

interface CompanyInterface
{
    public function getAll();

    public function getByCode(string $companyCode): ?CompanyEntity;
}