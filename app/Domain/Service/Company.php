<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:47 AM
 */

namespace App\Domain\Service;

use App\Domain\Repository\CompanyInterface as RepositoryCompanyInterface;
use App\Domain\Adapter\CacheInterface;
use App\Domain\Entity\Company as CompanyEntity;


class Company implements CompanyInterface
{
    const CACHE_KEY = 'all_countries';

    private $repository;

    private $cache;

    public function __construct(RepositoryCompanyInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function getAll(): array
    {
        $entities = [];

        if(($dataCompanies = $this->cache->get(self::CACHE_KEY)) === null) {
           $dataCompanies = $this->repository->getAll();

           if ($dataCompanies) {
               $this->cache->set(self::CACHE_KEY, $dataCompanies, 360);
           }
        }

        if ($dataCompanies) {
            foreach ($dataCompanies as $item) {
                $entities[] = $this->prepareEntity($item);
            }
        }

        return $entities;
    }

    public function getByCode(string $companyCode): ?CompanyEntity
    {
        $data = $this->repository->getByCode($companyCode);
        if ($data) {
            return $this->prepareEntity($data);
        }

        return null;
    }

    private function prepareEntity($data)
    {

        $entity = new CompanyEntity();
        $entity->setSymbol($data['Symbol']);
        $entity->setCompanyName($data['Company Name']);

        return $entity;
    }
}