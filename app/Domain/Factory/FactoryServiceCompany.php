<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:44 AM
 */

namespace App\Domain\Factory;

use App\Domain\Repository\PkgstoreApiCompanyRepository;
use App\Domain\Service\CompanyInterface as ServiceCompanyInterface;
use App\Domain\Service\Company as ServiceCompany;
use App\Domain\Adapter\LaravelCache as LaravelCacheAdapter;
use App\Domain\Adapter\GuzzleHttpClient as GuzzleHttpClientAdapter;

class FactoryServiceCompany
{
    public static function build(): ServiceCompanyInterface
    {
        $cache = new LaravelCacheAdapter();
        $httpClient = new GuzzleHttpClientAdapter();

        $service = new ServiceCompany(new PkgstoreApiCompanyRepository($httpClient), $cache);

        return $service;
    }
}