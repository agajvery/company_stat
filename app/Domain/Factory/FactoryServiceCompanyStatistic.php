<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 1:38 PM
 */

namespace App\Domain\Factory;

use App\Domain\Service\CompanyStatisticInterface as ServiceCompanyStatisticInterface;
use App\Domain\Service\CompanyStatistic as ServiceCompanyStatistic;
use App\Domain\Adapter\GuzzleHttpClient as GuzzleHttpClientAdapter;
use App\Domain\Repository\QuandlApiCompanyStatisticRepository;
use App\Domain\Repository\DummyApiCompanyStatisticRepository;

class FactoryServiceCompanyStatistic
{
    public static function build(): ServiceCompanyStatisticInterface
    {
//        $httpClient = new GuzzleHttpClientAdapter();
//        $repository = new QuandlApiCompanyStatisticRepository($httpClient);

        $repository = new DummyApiCompanyStatisticRepository();

        $service = new ServiceCompanyStatistic($repository);
        return $service;
    }
}