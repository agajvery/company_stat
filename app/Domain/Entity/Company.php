<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:34 AM
 */

namespace App\Domain\Entity;


class Company
{
    private $companyName;

    private $symbol;


    public function setSymbol(string $value)
    {
        $this->symbol = $value;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setCompanyName(string $value)
    {
        $this->companyName = $value;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }
}