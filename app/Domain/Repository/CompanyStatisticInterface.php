<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 2:11 PM
 */

namespace App\Domain\Repository;


interface CompanyStatisticInterface
{
    public function getStatistic(string $companyCode, string $dateFrom, string $dateTo): array;
}