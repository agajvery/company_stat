<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 12:41 PM
 */

namespace App\Domain\Repository;


use App\Domain\Adapter\HttpClientInterface;
use Exception;

abstract class ApiRepositoryAbstract
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    protected function request(string $method, string $url, array $params = [])
    {
        $response = $this->httpClient->request($method, $url, $params);
        if ($response['code'] == 200) {
            return json_decode($response['body'], true);
        } else {
            throw new Exception(sprintf("Response error code: %s", $response['code']));
        }
    }
}