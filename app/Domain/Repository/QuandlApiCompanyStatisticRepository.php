<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 2:13 PM
 */

namespace App\Domain\Repository;

use App\Domain\Adapter\HttpClientInterface;
use Exception;

class QuandlApiCompanyStatisticRepository extends ApiRepositoryAbstract implements CompanyStatisticInterface
{
    const ENDPOINT = 'https://www.quandl.com/api/v3/datasets/WIKI/{company_code}.json';

    public function getStatistic(string $companyCode, string $dateFrom, string $dateTo): array
    {
        try {
            $response = $this->request(
                HttpClientInterface::METHOD_GET,
                str_replace('{company_code}', $companyCode, self::ENDPOINT),
                [
                    'start_date' => $dateFrom,
                    'end_date' => $dateTo
                ]
            );

            return [
                'column_name' => $response['column_names'],
                'data' => $response['data']
            ];
        } catch (Exception $exception) {
            error_log($exception);
            return [];
        }
    }
}