<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:39 AM
 */

namespace App\Domain\Repository;

interface CompanyInterface
{
    public function getAll(): array;

    public function getByCode(string $companyCode): ?array;
}