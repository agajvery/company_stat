<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 8:41 AM
 */

namespace App\Domain\Repository;


use App\Domain\Adapter\HttpClientInterface;
use App\Domain\Entity\Company as EntityCompany;
use Exception;

class PkgstoreApiCompanyRepository extends ApiRepositoryAbstract implements CompanyInterface
{
    const ENDPOINT = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';

    public function getAll(): array
    {
        try {
            $data = $this->request(HttpClientInterface::METHOD_GET, self::ENDPOINT);
            return array_combine(array_column($data, 'Symbol'), $data);
        } catch (Exception $exception) {
            error_log($exception);
            return [];
        }
    }

    public function getByCode(string $companyCode): ?array
    {
        $allCompanies = $this->getAll();
        return $allCompanies[$companyCode] ?? null;
    }
}