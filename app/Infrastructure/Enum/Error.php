<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/12/20
 * Time: 8:44 AM
 */

namespace App\Infrastructure\Enum;


class Error
{
    const ERROR_VALIDATION = 1001;
    const ERROR_503 = 503;
}