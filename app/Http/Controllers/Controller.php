<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function error(int $errorCode, array $reasons)
    {
        return  response()->json([
            'error' => [
                'code' => $errorCode,
                'reason' => $reasons
            ]
        ]);
    }

    protected function success($payload = [])
    {
        return response()->json($payload);
    }
}
