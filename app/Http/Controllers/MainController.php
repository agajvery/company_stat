<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 4/11/20
 * Time: 10:32 PM
 */

namespace App\Http\Controllers;

use App\Providers\LaravelCache;
use Illuminate\Http\Request;
use App\Domain\Factory\FactoryServiceCompany;


class MainController extends Controller
{
    private $companyService;

    public function __construct()
    {
        $this->companyService = FactoryServiceCompany::build();
    }

    public function index(Request $request)
    {
        return view('welcome', ['companies' => $this->companyService->getAll()]);
    }
}