<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 5/3/20
 * Time: 1:33 PM
 */

namespace App\Http\Controllers\Statistic;


use App\Domain\Factory\FactoryServiceCompany;
use App\Domain\Factory\FactoryServiceCompanyStatistic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    private $statisticService;

    private $companyService;

    public function __construct()
    {
        $this->statisticService = FactoryServiceCompanyStatistic::build();
        $this->companyService = FactoryServiceCompany::build();
    }

    public function index(Request $request)
    {
        $validateData = $request->validate([
            'company_code' => 'required|max:5',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'email' => 'required|email:rfc'
        ]);

        if (isset($validateData['errors'])) {
            return response()->json(['success' => false, 'errors' => $validateData['errors']]);
        }

        $company = $this->companyService->getByCode($validateData['company_code']);
        if ($company === null) {
            return response()->json(['success' => false, 'error' => 'Invalid company symbol']);
        }

        $payload = $this->statisticService->getStatistic(
            $validateData['company_code'],
            $validateData['date_from'],
            $validateData['date_to']
        );

        $emailReceiver = $validateData['email'];
        $emailSubject = $company->getCompanyName();

        $startDate = $validateData['date_from'];
        $endDate = $validateData['date_to'];

        dispatch(function () use ($emailReceiver, $emailSubject, $startDate, $endDate) {
            Mail::send('emails.statistic', ['startDate' => $startDate, 'endDate' => $endDate], function ($message) use ($emailReceiver, $emailSubject) {
                $message->to($emailReceiver)->subject($emailSubject);
            });
        })->afterResponse();

        return response()->json(['success' => true, 'payload' => $payload]);
    }
}