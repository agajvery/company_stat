<?php

namespace App\Jobs;

use App\Domain\Factory\FactoryServiceCountryStatistic;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CountryStatisticViewIncrement implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $countryCode;

    /**
     * CountryStatisticViewIncrement constructor.
     * @param string $countryCode
     */
    public function __construct(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = FactoryServiceCountryStatistic::build();
        $service->incrementView($this->countryCode);
    }
}
