## Usage

Create and start containers. Lunch application

```/deploy.sh up```

Stop and remove containers

```./deploy.sh down```
