import $ from 'jquery';
import datepickerFactory from 'jquery-datepicker';
import jqValidator from 'jquery-validation';
import Chart from 'chart.js';

var displayFields = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'];

datepickerFactory($);
jqValidator($);

$(function() {

    //init datapicker
    var dateFormat = "yy-mm-dd",
        from = $( "#date-from" )
            .datepicker({
                defaultDate: "2018-01-01",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on( "change", function() {
                to.datepicker( "option", "minDate", getDate( this ) );
            }),

        to = $( "#date-to" ).datepicker({
            defaultDate: "2018-01-01",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: dateFormat
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });

    //set validation rules
    $("#stat-form").validate({
        rules: {
            date_from: {
                required: true,
            },
            date_to: {
                required: true,
            },
            email: {
                email: true,
                required: true
            },
            company_code: {
                required: true
            }

        },
        errorClass: "form-error",
        errorElement: "div"
    });


    //submit form
    $("#btn-submit-form").click(function(){
        let jqForm = $("#stat-form");

        if (jqForm.valid()) {
            $.ajax({
                url: jqForm.attr("action"),
                data: jqForm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        showResult(response.payload);
                        showChart(response.payload);
                    }
                }
            });
        }
    });

    function showResult(rows) {
        let html = "<table>";

        html += ('<thead><tr><th>' + displayFields.join('</th><th>') + '</th></tr></thead>');

        html += '<tbody>';
        for (let index in rows) {
            let row = rows[index];

            html += '<tr>';
            for (let keyIndex in displayFields) {
                html += ('<td>' + row[displayFields[keyIndex]] + '</td>');
            }
            html += '</tr>';
        }

        html += "</tbody></table>";

        $("#table-content").html(html);
    }

    function showChart(data) {
        var labels = [],
            openPrice = [],
            closePrice = [];

        for (let index in data) {
            let row = data[index];

            labels.push(row.Date);
            openPrice.push(row.Open);
            closePrice.push(row.Close);
        }


        var data = {
            labels: labels,
            datasets: [{
                label: "Open prices",
                fill: false,
                lineTension: 0.1,
                borderColor: "red",
                borderCapStyle: 'square',
                data: openPrice,
                spanGaps: true,
            }, {
                label: "Close prices",
                fill: true,
                lineTension: 0.1,
                borderColor: "rgb(167, 105, 0)",
                borderCapStyle: 'butt',
                data: closePrice,
                spanGaps: false,
            }

            ]
        };

        var options = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Moola',
                        fontSize: 20
                    }
                }]
            }
        };


        var ctx = document.getElementById('priceChart');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });
    }


    function getDate( element ) {
        var date;

        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }

        return date;
    };
});

