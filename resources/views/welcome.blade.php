<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4">
                        <form id="stat-form" method="post" action="/api/company/statistic">
                            @csrf
                            <div class="form-group">
                                <label for="company-code">Company Symbol</label>
                                <select name="company_code" id="company-code" class="form-control" required>
                                    @foreach ($companies as $company)
                                        <option value="{{ $company->getSymbol() }}" >{{ $company->getSymbol() }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="company-symbol">Date</label>
                                <div class="input-group input-daterange">
                                    <div class="input-group-addon">From</div>
                                    <input
                                            id="date-from"
                                            name="date_from"
                                            type="text"
                                            class="form-control"
                                            placeholder="Enter date from"
                                            required
                                    />

                                    <div class="input-group-addon"> to </div>
                                    <input id="date-to" name="date_to" type="text" class="form-control" placeholder="Enter date to" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company-symbol">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" type="email" required>
                            </div>
                            <button type="button" id="btn-submit-form" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">
                        <div id="table-content"></div>
                    </div>
                    <div class="col-sm-10">
                        <canvas id="priceChart" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>

</html>
